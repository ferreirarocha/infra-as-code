#!/bin/bash


# Definindo a senha padrão

SENHA_PADRAO="senhaPadrao123"


# Definindo o caminho dos arquivos de usuários, grupos e senhas

ARQUIVO_USUARIOS="usuarios.txt"

ARQUIVO_GRUPOS="grupos.txt"

ARQUIVO_SENHAS="senhas.txt"

# Verificando se os arquivos de usuários e grupos existem

if [ ! -f "$ARQUIVO_USUARIOS" ] || [ ! -f "$ARQUIVO_GRUPOS" ]; then

    echo "Erro: Um ou mais arquivos não foram encontrados."

    exit 1

fi


# Criando grupos de usuários

while IFS= read -r grupo; do

    groupadd "$grupo"

done < "$ARQUIVO_GRUPOS"


# Criando usuários, definindo senhas e adicionando-os aos grupos

while IFS= read -r linha; do

    usuario=$(echo "$linha" | awk '{print $1}')

    useradd -m "$usuario" -s /bin/bash  # -m para criar diretório home para o usuário

    echo "$usuario:$SENHA_PADRAO" | chpasswd  # Definindo a senha padrão para o usuário

    # Adicionando o usuário aos grupos

    grupos=$(echo "$linha" | awk '{$1=""; print $0}')  # Obtendo os grupos do usuário

    for grupo in $grupos; do

        usermod -aG "$grupo" "$usuario"

    done

    # Salvando usuário e senha no arquivo de senhas

    echo "$usuario:$SENHA_PADRAO" >> "$ARQUIVO_SENHAS"

    chage -d 0 $usuario

done < "$ARQUIVO_USUARIOS"


# Definindo a flag para que os usuários alterem suas senhas no próximo login



# Definindo as permissões para os diretórios home dos usuários

for usuario in $(cut -d' ' -f1 "$ARQUIVO_USUARIOS"); do

    chown -R "$usuario:$usuario" "/home/$usuario"

    chmod 700 "/home/$usuario"

done


echo "Infraestrutura de usuários, grupos e senhas criada com sucesso!"

